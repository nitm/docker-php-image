# syntax=docker/dockerfile:experimental
FROM php:8.2-cli
RUN apt-get update  \
        && apt-get install -y -qq --no-install-recommends wget gnupg2 apt-transport-https lsb-release ca-certificates software-properties-common  \
        && wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg \
        && rm /etc/apt/preferences.d/no-debian-php \
        && echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list \
        && ls -l /etc/apt/* \
        && mkdir -p /etc/apt/keyrings; \
        curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key \
        | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg; \
        NODE_MAJOR=18; \
        echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" \
        > /etc/apt/sources.list.d/nodesource.list \
        && apt-get update  \
        && apt-get install -y -qq --no-install-recommends git openssh-client apt-transport-https lsb-release ca-certificates groff jq curl build-essential libzip-dev libcurl4 libcurl4-openssl-dev nodejs software-properties-common php8.2-intl php8.2-gd git php8.2-cli php8.2-curl php8.2-pgsql php8.2-ldap php8.2-sqlite3 php8.2-mysql php8.2-zip php8.2-xml php8.2-mbstring php8.2-dev php8.2-gd php8.2-exif php8.2-readline php8.2-sqlite3 make libmagickcore-6.q16-2-extra unzip php8.2-redis php8.2-imagick php8.2-dev php8.2-bcmath php8.2-mongodb libsystemd-dev python3 python3-pip python3-dev libpng-dev python3-setuptools pipx libicu-dev \
        nodejs libsodium-dev \
        && apt-get autoremove -y  \
        && apt-get autoclean  \
        && apt-get clean
RUN docker-php-ext-install exif gd sodium pcntl curl zip intl bcmath
RUN docker-php-ext-enable exif gd sodium pcntl curl zip intl bcmath
RUN cd /tmp/  \
        && wget https://github.com/nikic/php-ast/archive/master.zip  \
        && unzip master.zip
RUN cd /tmp/php-ast-master/  \
        && phpize  \
        && ./configure  \
        && make  \
        && make install  \
        &&rm -rf /tmp/php-ast-master/
RUN echo "extension=ast.so" >> /etc/php/8.2/cli/conf.d/20-ast.ini
RUN cd /tmp  \
        && wget -O php-systemd-src.zip https://github.com/systemd/php-systemd/archive/master.zip  \
        && unzip php-systemd-src.zip  \
        && cd /tmp/php-systemd-master  \
        && phpize  \
        && ./configure --with-systemd  \
        && make  \
        && make install  \
        && rm -rf /tmp/php-systemd-master  \
        && echo "extension=systemd.so" >> /etc/php/8.2/mods-available/systemd.ini
RUN phpenmod zip intl gd systemd exif
RUN curl -O -L https://phar.phpunit.de/phpunit-9.5.4.phar  \
        && chmod +x phpunit-9.5.4.phar  \
        && mv phpunit-9.5.4.phar /usr/local/bin/phpunit
RUN curl -O -L https://getcomposer.org/composer-stable.phar  \
        && chmod +x composer-stable.phar  \
        && mv composer-stable.phar /usr/local/bin/composer
RUN phpdismod xdebug
RUN npm i -g npm
RUN pip3 --version
RUN ls -l /usr/lib/python*
RUN pipx install awsebcli==3.19.4
RUN pipx install awscli
RUN pipx install cloudflare
RUN curl -sSL https://sdk.cloud.google.com | bash
ENV PATH=~/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root/google-cloud-sdk/bin
RUN echo $PATH