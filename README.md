# Build Image

```
docker build -t ninjasitm/php${VERSION}-cli:latest .
```

# Push Image

```
docker push ${TAG} ninjasitm/php${VERSION}-cli:latest
```
